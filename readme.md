Config tomcat-users.xml :

<?xml version="1.0" encoding="UTF-8"?>
<tomcat-users>	
	<role rolename="secured" />
	<user password="password" roles="secured" username="admin" />	
</tomcat-users>